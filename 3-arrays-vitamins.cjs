const items = [{
    name: 'Orange',
    available: true,
    contains: "Vitamin C",
}, {
    name: 'Mango',
    available: true,
    contains: "Vitamin K, Vitamin C",
}, {
    name: 'Pineapple',
    available: true,
    contains: "Vitamin A",
}, {
    name: 'Raspberry',
    available: false,
    contains: "Vitamin B, Vitamin A",

}, {
    name: 'Grapes',
    contains: "Vitamin D",
    available: false,
}];


/*

    1. Get all items that are available 
    2. Get all items containing only Vitamin C.
    3. Get all items containing Vitamin A.
    4. Group items based on the Vitamins that they contain in the following format:
        {
            "Vitamin C": ["Orange", "Mango"],
            "Vitamin K": ["Mango"],
        }
        
        and so on for all items and all Vitamins.
    5. Sort items based on number of Vitamins they contain.

    NOTE: Do not change the name of this file

*/



// 1. Get all items that are available 


function getItemAvaiable() {

    const getItem = items.map((data) => {
        return data;
    })

    console.log(getItem);

}
getItemAvaiable();




// //  2. Get all items containing only Vitamin C.


function getItemWithVitaminC() {

    const getVitaminC = items.filter((data) => {
        if (data.contains.includes("Vitamin C")) {
            return data;
        }
    })

    console.log(getVitaminC);
}
getItemWithVitaminC();



// // 3. Get all items containing Vitamin A.

function getItemWithVitaminA() {

    const getVitaminA = items.filter((data) => {
        if (data.contains.includes("Vitamin A")) {
            return data;
        }
    })

    console.log(getVitaminA);
}
getItemWithVitaminA();


// 4 Group items based on the Vitamins that they contain in the following format:


function getGroupVitaminItems() {

    const groupVitamins = items.reduce((accum, data) => {

        const array = data.contains.split(', ');

        array.reduce((accum1, item) => {

            if (accum[item]) {
                accum[item].push(data.name);
            }
            else {
                accum[item] = [data.name];
            }
            return accum;

        }, {});

        // if(data.contains.includes(accum[data.contains]))
        // {
        //     accum[data.contains]=[...accum[data.contains],data.name];

        // }
        // else{
        //     accum[data.contains]=data.name
        // }
        return accum;

    }, {});

    console.log(groupVitamins);
}
getGroupVitaminItems()






//  5. Sort items based on number of Vitamins they contain.

function sortItemOnVitamins() {

    const sortVitamins = items.sort((vitamin1, vitamin2) => {

        const vitamindata1 = vitamin1.contains.split(', ');
        const vitamindata2 = vitamin2.contains.split(', ');

        if (vitamindata1.length > vitamindata2.length) {
            return 1;
        }
        else if (vitamindata1.length < vitamindata2.length) {
            return -1;
        }
        else {
            return 0;
        }



    })


    console.log(sortVitamins);

}
sortItemOnVitamins();